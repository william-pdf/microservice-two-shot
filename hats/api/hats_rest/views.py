from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
from .models import Hat, LocationVO
import json

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "section_number", "shelf_number"]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["name", "style", "location"]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name", 
        "fabric", 
        "style", 
        "color", 
        "picture_url", 
        "location",
    ]
    encoders = {"location": LocationVOEncoder}


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
